canvas = document.getElementById("canvas")
ctx = canvas.getContext
colorpicker = $("#colorpicker")

editview = false
clearing = true
brush = "linear"
selecting = false
filling = true
drawing = false
game = false

mainloop = true

rate = 0.008
color = "#aaa"
bg_color = "#000"
clear_amount = 1
random_size = 5
size = 20
rotation = 0
line_size = 2
line_vibe = 5
poly_count = 4
direction = 1

blank_project = () -> {forms : [], bg : "#000", ca: 1, rate: 0.008}
selected_color = () -> $("#colorpicker").css("background-color")
selected_color2 = () -> $("#colorpicker2").css("background-color")
project = blank_project()

first = {x:-size, y:-size}
mousePos = getMousePos
lastUpdate = Date.now
frame = 0
removable = -1
rad = Math.PI/180.0
selection = 0
tick = 0

snakeBuffer = []

`function getMousePos(c, evt){
  var rect = c.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}`

resizeCanvas = () ->
  ctx.canvas.width  = window.innerWidth
  ctx.canvas.height = window.innerHeight

init = () ->
  ctx = canvas.getContext "2d"
  mainloop = window.setInterval main, 16.666
  resizeCanvas()
  window.addEventListener 'resize', resizeCanvas
  # toggle_info()

rungame = ()->
  if pick_color(mousePos).b > 0 then log "failing"

pick_color = (mp) ->
  pixel = ctx.getImageData Math.round(mp.x), Math.round(mp.y), 1, 1
  data = pixel.data
  { r:data[0], g:data[1],b: data[2]}
main = () ->
  if clearing then clear()
  if tick > 1 then tick = 0 else tick += rate
  render tick
  if brush is "snake" and drawing then snake()
  if removable isnt -1 then remove removable
  if game then rungame()
  # frame++

render = (t) ->
  d = new Date()
  d = d.getTime()

  for o in project.forms
    # if editview and o.t isnt "snake"then draw_gizmos o
    switch o.t
      when "linear", "circular", "bouncer" then draw_poly o, getPosition( o, t, d), o.pc
      when "line" then stroke o, o.c, o.rs, o.s
      when "snake" then draw_snake o, t

  if selecting && project.forms.length>0 then select project.forms[selection], t
  if drawing 
    stroke({a : first, b : mousePos}, random_rgb(), line_vibe, if brush is "line" then line_size else 2)
  if brush is "snake" then draw_line snakeBuffer
  switch brush
    when "linear", "circular", "bouncer" then draw_poly {s:size, rs: (if filling then random_size else line_vibe), fill: filling, rot: rotation, ls:line_size, c:selected_color()}, mousePos, poly_count
    when "line", "snake"
      draw_poly {s:line_size/2,rot:0, rs:line_vibe, fill: true, c:selected_color()}, mousePos, 4
  if editview
    for o in project.forms
      if o.t isnt "snake" and o.t isnt "line" then draw_gizmos o
getPosition = (o, t, d) ->
    switch o.t
      when "circular" then rotate_point o.b, o.a, o.dir*t*360
      when "linear", "line" then lerp2d o, t
      when "bouncer" then lerp2d o, 0.5-Math.sin(t*2*Math.PI+11.6)*0.5

lerp2d = (p, t) -> {x:p.a.x*(1-t)+p.b.x*t, y:p.a.y*(1-t)+p.b.y*t}

lerp = (a, b, t) -> a*(1-t)+b*t

random = () -> lerp -1, 1, Math.random()

rotate_point = (p, o, angle) ->
  angle *= rad
  _x = Math.cos(angle) * (p.x-o.x) - Math.sin(angle) * (p.y-o.y) + o.x
  _y = Math.sin(angle) * (p.x-o.x) + Math.cos(angle) * (p.y-o.y) + o.y
  { x: _x, y: _y}

$("html").keydown (e) ->
  # log "pressed "+e.which
  switch e.which
    when 49,50,51,52,53,54,55,56,57,58 then files e.which, e.shiftKey
    when 83 then selecting = !selecting if project.forms?
    when 8, 90 then undo()
    when 81 then toggle_info()
    when 69 then editview = !editview
    when 87 then clearing = !clearing
    when 80 then log JSON.stringify project 

    when 85 then game = !game

  if not selecting
    switch e.which
      when 68 then brush = "snake"
      when 88 then brush = "line"
      when 67 then brush = "circular"
      when 86 then brush = "linear"
      when 66 then brush = "bouncer"
      when 37 then set_randomsize(-1)
      when 39 then set_randomsize(1)
      when 38 then set_size 1
      when 40 then set_size -1
      when 84 then direction *= -1
      when 72 then rotation-=1
      when 74 then rotation+=1
      when 70 then filling = !filling
      when 71 then project.bg = selected_color()
      when 73 then poly_count = clamp poly_count-1, 2, 32 
      when 79 then poly_count = clamp poly_count+1, 2, 32 
      when 75 then project.ca = clamp project.ca-0.01, 0, 1
      when 76 then project.ca = clamp project.ca+0.01, 0, 1
  if selecting
    switch e.which
      when 82 then removable = selection
      when 37 then shift_selection -1
      when 39 then shift_selection 1
      when 38 then resize 1 
      when 40 then resize -1

  $("#brush").html(brush)

set_size = (i) -> if filling and brush isnt "line" and brush isnt "snake" then size += i else line_size = clamp line_size+i, 1, 1000
set_randomsize = (i) -> if filling and brush isnt "line" and brush isnt "snake" then random_size = clamp random_size+i, 0, 1000 else line_vibe = clamp line_vibe+i, 0, 1000
$("#canvas").mousemove (e) -> mousePos = getMousePos canvas, e if canvas?
$("#canvas").mousedown (e) -> if e.which is 1 then start_draw(mousePos)
$("#canvas").mouseup (e) -> if e.which is 1 then end_draw() else log "failed to add"

toggle_info = ()->
      $("#controls").animate 
        opacity: (1.0-$("#controls").css "opacity")
        left: (-30*($("#controls").css "opacity"))+"em"
        'fast', ->#

snake = () -> 
  snakeBuffer.push {a:first, s:line_size, b:mousePos, c: selected_color(), line_vibe : line_vibe}
  first = mousePos

start_draw = (mouseP) ->
  drawing = true
  if brush is "snake" then snakeBuffer = []
  first = mouseP
end_draw = () ->
  add()
  drawing = false
add = () ->
  # log "adding"
  switch brush
    when "line" then project.forms.push {a:first, s:line_size, b:mousePos,t: brush, c:selected_color(), rs : line_vibe}
    when "snake"
      if snakeBuffer.length > 0 
        project.forms.push {t:"snake", s:size, ls: line_size, rs:line_vibe, list : snakeBuffer, c: selected_color()}
        snakeBuffer = []
    else project.forms.push {a:first, b:mousePos, fill:filling, rot:rotation, ls:line_size, pc: poly_count, s:size, rs:(if filling then random_size else line_vibe), c:selected_color(), c2: selected_color2(), t:brush, dir:direction}

undo = () -> 
  selecting = false
  if project.forms.length>0 then project.forms.pop() else false

random_rgb = () -> hsl Math.floor(Math.random()*35)*120, 100, 50

draw_poly = (o, pos, c) ->
  ctx.lineWidth = o.ls
  ctx.strokeStyle = o.c
  poly = []
  for i in [0..c]
    poly.push rotate_point( {x:pos.x+o.s+((Math.random()*2)-1)*o.rs, y:pos.y+o.s+((Math.random()*2)-1)*o.rs}, pos, (360/c)*i+o.rot )#+((Math.random()*2)-1)*o.rs)
  # ctx.fillStyle = o.c
  ctx.fillStyle = o.c
  ctx.beginPath()
  p = rotate_point( {x:pos.x+o.s+((Math.random()*2)-1)*o.rs, y:pos.y+o.s+((Math.random()*2)-1)*o.rs}, pos, (360/c)*0+o.rot )
  ctx.moveTo p.x, p.y
  for j in [1..c]
    p = rotate_point( {x:pos.x+o.s+((Math.random()*2)-1)*o.rs, y:pos.y+o.s+((Math.random()*2)-1)*o.rs}, pos, (360/c)*j+o.rot )
    if j is c then ctx.closePath() else ctx.lineTo p.x, p.y
  if o.fill then ctx.fill() else ctx.stroke()

draw_snake = (sn, time) ->
  len = sn.s
  num = Math.round(sn.list.length * time)
  if num < len
    len = Math.round(clamp(num+time*len, 0, sn.list.length))
  else 
    len = clamp(num+len, 0, sn.list.length)
  for num  in [num..len]
    stroke( sn.list[clamp( num, 0, sn.list.length-1)], sn.c, sn.rs, sn.ls)

draw_line = (sn) ->
  cc = random_rgb()
  for n in sn
    stroke(n, cc, 0, 2)
    
draw_gizmos = (o) ->
  ctx.lineWidth = 1
  ctx.strokeStyle = random_rgb()
  ctx.fillStyle = ctx.strokeStyle
  ctx.strokeRect o.a.x-3,o.a.y-3, 6, 6
  ctx.fillRect o.b.x-3,o.b.y-3, 6, 6 
  true

stroke = (l, c, rs, ls) ->
  ctx.lineWidth = ls
  ctx.strokeStyle = c
  ctx.beginPath()
  ctx.moveTo l.a.x + (rs*random()), l.a.y + (rs*random())
  ctx.lineTo l.b.x + (rs*random()), l.b.y + (rs*random())
  ctx.stroke()

select = (o, t, d) ->
  if o.t isnt "snake"
    # log o.t
    d = new Date().getTime()
    ctx.lineWidth = 3
    ctx.strokeStyle = hsl Math.floor(Math.random()*35)*120, 100, 50
    ss = o.s+(o.s+Math.sin(d*0.01)*o.s*0.25)
    sm = if o.t is "line" then getPosition o, 0.5, d else getPosition o, t, d
    if o.t is "line" then ctx.strokeRect(sm.x - Math.abs(o.a.x - o.b.x)*0.5, sm.y - Math.abs(o.a.y - o.b.y)*0.5, Math.abs(o.a.x - o.b.x),Math.abs(o.a.y - o.b.y)) else ctx.strokeRect sm.x-ss*0.5, sm.y-ss*0.5, ss, ss
  else draw_line o.list

shift_selection = (i) ->  
  selection =  if i is 1 then (selection+1) % project.forms.length else `selection-1 < 0 ? project.forms.length-1 : selection-1`
  selection = clamp(selection, 0, project.forms.length)
  if project.forms.length == 0 then selection = 0

resize = (i) -> project.forms[selection].s += i if project.forms.length>0

remove = (r) ->
    selecting = false
    project.forms.splice r, 1
    removable = -1
    shift_selection 1

files = (n, shift) -> if shift then save n else load n
save = (n) -> localStorage.setItem(n+"", JSON.stringify(project))
load = (n) -> project =  if localStorage.getItem(n+"")? then JSON.parse(localStorage.getItem n+"") else blank_project()
log = (s) -> console.log s
clear = () ->
    ctx.fillStyle = project.bg
    ctx.globalAlpha = project.ca;
    # ctx.fillRect(50,50,75,50);
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    ctx.globalAlpha=1;
clamp = (x, a, b) -> if x<a then a else if x>b then b else x
rgb = (r,g,b) -> "rgb("+r+","+g+","+b+")"
RGB = (c) -> "rgb("+c.r+","+c.g+","+c.b+")"
hsl = (h,s,l) -> "hsl("+h+","+s+"%,"+l+"%)"
# collide = (m, r) -> (m.x>r.x and m.x<r.x+r.w and m.y>r.y and m.y<r.y+r.h)
